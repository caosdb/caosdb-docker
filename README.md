# Welcome
**THIS REPOSITORY IS DEPRECATED! PLEASE USE LinkAhead Control!**

https://gitlab.com/linkahead/linkahead-control

**Migration to LinkAhead Control:**

```bash
# log into the container
docker exec -ti compose_caosdb-server_1 bash
# go to the sql folder
cd git/caosdb-mysqlbackend/
# create the sql dump
utils/backup.py -d /tmp/backup
# create the tar archive containing files, that were uploaded
tar czvf /tmp/backup/backup.tar.gz /opt/caosdb/mnt/caosroot
# log out of the container (Ctrl-D)
# then copy the backup from the container
docker cp compose_caosdb-server_1:/tmp/backup .
```
You can use these two files to restore your data using LinkAhead Control.


This is **CaosDB Docker**, a part of the CaosDB project.

If you want to try out CaosDB, you are at the right place.  For further
instruction and a 1-step-demo, please continue reading in
[README_SETUP.md](README_SETUP.md).


# Further Reading

Please refer to the [official gitlab repository of the CaosDB
project](https://gitlab.indiscale.com/caosdb/src) for more information.

# License

Copyright (C) 2022 IndiScale GmbH
Copyright (C) 2022 Daniel Hornung, Timm Fitschen, Henrik tom Wörden.

All files in this repository are licensed under the [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

