# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.9.2] - 2023-02-28
(Henrik tom Wörden)

### Changed
* Update CaosDB components to their most recent releases

## [0.9.1] - 2022-11-08
(Florian Spreckelsen)

### Fixed

* Update CaosDB components to their most recent releases

## [0.9.0] - 2022-08-30
(Henrik tom Wörden)

### Added

* a `Dockerfile` to build the CaosDB docker image and a `docker-compose.yml`
  that allows to start the CaosDB server.

### Changed

### Deprecated

### Removed

### Fixed

### Security

